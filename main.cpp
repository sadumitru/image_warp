#include <iostream>
#include <cmath>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>

#define MAX_WIDTH 512
#define MAX_HEIGHT 512

static void my_filter(const unsigned char *input_buffer, unsigned char *output_buffer) {
    for (int i = 0; i < 512; i++) {
        for (int j = 0; j < 512; j++) {
            int offset_x = (int) (15.0 * sin(2 * 3.14 * i / 130));
            int offset_y = (int) (15.0 * cos(2 * 3.14 * j / 130));
            if ((j + offset_x < 512) && (i + offset_y < 512)) {
                output_buffer[i + j * 512] = input_buffer[(i + offset_y) % 512 +
                                                          ((j + offset_x) % 512) * 512];
            } else {
                output_buffer[i + 512 * j] = 0;
            }
        }
    }
}


double my_SRN (const cv::Mat& output_jpeg) {
    cv::Scalar meanOfImage, stdDev;
    cv::meanStdDev(output_jpeg, meanOfImage, stdDev);
    return meanOfImage(0)/stdDev(0);
}

int main() {
    FILE *input_image = fopen("../lena_512x512_luma.raw", "rb");

    const int width = MAX_WIDTH;
    const int height = MAX_HEIGHT;

    auto *input_buffer = (unsigned char *) new char *[sizeof(unsigned char) * width * height];
    auto *output_buffer = (unsigned char *) new char *[sizeof(unsigned char) * width * height];

    unsigned int bytesLoaded = fread(input_buffer, 1, width * height, input_image);

    if (bytesLoaded != width * height) {
        if (feof(input_image)) {
            printf("End of file!\n");
        }

        if (ferror(input_image)) {
            printf("Error reading from file.\n");
        }

        printf("Could not read from stream enough bytes.\n");
        printf("Expected %u, loaded %u bytes\n", width * height, bytesLoaded);
        exit(1);
    }

    my_filter(input_buffer, output_buffer);

    cv::Mat output_image = cv::Mat(height, width, CV_8U, output_buffer).clone();
    cv::imwrite("../output.jpeg", output_image);

    std::cout<<"SRN of compressed image: "<<my_SRN(output_image);

    free(input_buffer);
    free(output_buffer);

    fclose(input_image);

    return 0;
}
